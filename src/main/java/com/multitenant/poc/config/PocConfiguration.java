package com.multitenant.poc.config;

import com.multitenant.poc.routing.CustomRoutingDataSource;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.boot.orm.jpa.EntityManagerFactoryBuilder;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.jdbc.datasource.DriverManagerDataSource;
import org.springframework.orm.jpa.JpaTransactionManager;
import org.springframework.orm.jpa.LocalContainerEntityManagerFactoryBean;
import org.springframework.transaction.PlatformTransactionManager;
import org.springframework.transaction.annotation.EnableTransactionManagement;

import javax.persistence.EntityManagerFactory;
import javax.sql.DataSource;
import java.util.HashMap;
import java.util.Map;
import java.util.Properties;

@Configuration
@EnableTransactionManagement
@EnableJpaRepositories(basePackages = {"com.multitenant.poc.repositories"}
    ,entityManagerFactoryRef = "pocEntityManagerFactory"
    ,transactionManagerRef = "pocTransactionManager"
)
public class PocConfiguration {

    @Bean
    @ConfigurationProperties(prefix = "spring.datasource1")
    public Properties poc1DataSourceProperties() {
        System.out.println("---poc1DataSourceProperties---");
        return new Properties();
    }

    public DataSource poc1DataSource() {
        System.out.println("---poc1DataSource---");
        DriverManagerDataSource dataSource = new DriverManagerDataSource();
        String driverClassName = poc2DataSourceProperties().getProperty("driver-class-name");
        if (driverClassName == null || driverClassName.isEmpty()) {
            driverClassName = poc2DataSourceProperties().getProperty("driverClassName");
        }
        dataSource.setDriverClassName(driverClassName);
        dataSource.setUrl(poc1DataSourceProperties().getProperty("url"));
        dataSource.setUsername(poc1DataSourceProperties().getProperty("username"));
        dataSource.setPassword(poc1DataSourceProperties().getProperty("password"));
        return dataSource;
    }

    @Bean
    @ConfigurationProperties(prefix = "spring.datasource2")
    public Properties poc2DataSourceProperties() {
        System.out.println("---poc1DataSourceProperties---");
        return new Properties();
    }

    public DataSource poc2DataSource() {
        DriverManagerDataSource dataSource = new DriverManagerDataSource();
        String driverClassName = poc2DataSourceProperties().getProperty("driver-class-name");
        if (driverClassName == null || driverClassName.isEmpty()) {
            driverClassName = poc2DataSourceProperties().getProperty("driverClassName");
        }
        dataSource.setDriverClassName(driverClassName);
        dataSource.setUrl(poc2DataSourceProperties().getProperty("url"));
        dataSource.setUsername(poc2DataSourceProperties().getProperty("username"));
        dataSource.setPassword(poc2DataSourceProperties().getProperty("password"));
        return dataSource;
    }

    @Bean
//    @Primary
    public DataSource dataSource() {
        System.out.println("---dataSource---");
        CustomRoutingDataSource dataSource = new CustomRoutingDataSource();

        Map<Object, Object> targetDataSources = new HashMap<>();

        targetDataSources.put("tenant1", poc1DataSource());
        targetDataSources.put("tenant2", poc2DataSource());

        dataSource.setTargetDataSources(targetDataSources);

        System.out.println("---dataSource---");
        return dataSource;
    }

    @Bean(name = "pocEntityManagerFactory")
    public LocalContainerEntityManagerFactoryBean pocEntityManagerFactory(EntityManagerFactoryBuilder builder
            ,  DataSource dataSource) {//@Qualifier("pocDataSource")
        return builder.dataSource(dataSource)
                .packages( "com.multitenant.poc.model")
                .persistenceUnit("poc")
                .build();
    }

    @Bean(name = "pocTransactionManager")
    public PlatformTransactionManager pocTransactionManager(
            @Qualifier("pocEntityManagerFactory") EntityManagerFactory pocEntityManagerFactory) {
        return new JpaTransactionManager(pocEntityManagerFactory);
    }

}
