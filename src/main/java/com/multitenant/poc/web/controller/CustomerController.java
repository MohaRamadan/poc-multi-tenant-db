package com.multitenant.poc.web.controller;

import com.multitenant.poc.model.Customer;
import com.multitenant.poc.web.CustomerService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
public class CustomerController {

    @Autowired
    private CustomerService customerService;

    @GetMapping("customer")
    public ResponseEntity<List<Customer>> getAllCustomers() {
        System.out.println("getAllCustomers");
        List<Customer> customerList = customerService.getAllCustomers();

        System.out.println("customerList"+ customerList);

        if(customerList==null)
            return new ResponseEntity<List<Customer>>(customerList, HttpStatus.NO_CONTENT);

        return new ResponseEntity<List<Customer>>(customerList, HttpStatus.OK);
    }

}
